<?php

use Faker\Generator as Faker;

$factory->define(Orders\Models\OrderModel::class, function (Faker $faker) {

    return [
        'name'      => $faker->unique()->name,
        'email'     => $faker->unique()->safeEmail,
        'address'   => $faker->address,
        'telephone' => $faker->phoneNumber,
        'gender'    => rand(0, 1) ? 'M' : 'F'
    ];

});
