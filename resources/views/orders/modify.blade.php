@extends('layouts.main')

@section('title', 'Modify order')

@section('content')

    <div class="card m-2">
        <div class="card-header font-weight-bold">
            Modify order
        </div>
        <div class="card-body">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form class="row" method="post" action="{{url('/orders/' . $id)}}">
                @method('PUT')
                <input type="hidden" value="{{csrf_token()}}" name="_token">

                <div class="form-group row col-sm-12 col-md-6">
                    <label for="name" class="col-sm-3 col-form-label">Name</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="name" name="name" placeholder="Enter name"
                               value="{{ old('name') ?? $name }}">
                    </div>
                </div>
                <div class="form-group row col-sm-12 col-md-6">
                    <label for="email" class="col-sm-3 col-form-label">E-mail</label>
                    <div class="col-sm-9">
                        <input type="email" class="form-control" id="email" name="email" placeholder="Enter email"
                               value="{{ old('email') ?? $email }}">
                    </div>
                </div>
                <div class="form-group row col-sm-12 col-md-6">
                    <label for="address" class="col-sm-3 col-form-label">Address</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="address" name="address" placeholder="Enter address"
                               value="{{ old('address') ?? $address }}">
                    </div>
                </div>
                <div class="form-group row col-sm-12 col-md-6">
                    <label for="telephone" class="col-sm-3 col-form-label">Telephone</label>
                    <div class="col-sm-9 ">
                        <input type="text" class="form-control" id="telephone" name="telephone"
                               placeholder="Enter telephone" value="{{ old('telephone') ?? $telephone }}">
                    </div>
                </div>
                <div class="form-group row col-sm-12 col-md-6">
                    <label for="gender" class="col-sm-3 col-form-label">Gender</label>
                    <div class="col-sm-9">
                        <select class="form-control col-3" id="gender" name="gender">
                            <option>M</option>
                            <option
                                    @if ( ( old('gender') ?? $gender ) === 'F')
                                    selected
                                    @endif
                            >
                                F
                            </option>
                        </select>
                    </div>
                </div>
                <div class="form-group row col-12">
                    <label for="additional_details" class="col-12 col-form-label">Additional details</label>
                    <div class="col-12">
                        <textarea class="form-control" id="additional_details" name="additional_details"
                                  rows="3">{{ old('additional_details') ?? $additional_details }}</textarea>
                    </div>
                </div>
                <div class="col-12 text-center">
                    <button type="submit" class="btn btn-primary">Modify</button>
                </div>
            </form>
        </div>
    </div>

@endsection