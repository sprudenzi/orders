@extends('layouts.main')

@section('title', 'Order details')

@section('content')

    <div class="card m-2">
        <div class="card-header font-weight-bold">
            Order n. {{ $order->id }}
        </div>
        <ul class="list-group list-group-flush">
            <li class="list-group-item">
                <div class="row">
                    <div class="col-xs-12 col-md-4 font-weight-bold"> Name: </div>
                    <div class="col-xs-12 col-md-8"> {{ $order->name }}</div>
                </div>
            </li>
            <li class="list-group-item">
                <div class="row">
                    <div class="col-xs-12 col-md-4 font-weight-bold"> E-mail: </div>
                    <div class="col-xs-12 col-md-8">{{ $order->email }} </div>
                </div>
            </li>
            <li class="list-group-item">
                <div class="row">
                    <div class="col-xs-12 col-md-4 font-weight-bold"> Address: </div>
                    <div class="col-xs-12 col-md-8">{{ $order->address }} </div>
                </div>
            </li>
            <li class="list-group-item">
                <div class="row">
                    <div class="col-xs-12 col-md-4 font-weight-bold"> Telephone: </div>
                    <div class="col-xs-12 col-md-8">{{ $order->telephone }} </div>
                </div>
            </li>
            <li class="list-group-item">
                <div class="row">
                    <div class="col-xs-12 col-md-4 font-weight-bold"> Gender: </div>
                    <div class="col-xs-12 col-md-8">{{ $order->gender }} </div>
                </div>
            </li>
            <li class="list-group-item">
                <div class="row">
                    <div class="col-xs-12 col-md-4 font-weight-bold"> Additional details: </div>
                    <div class="col-xs-12 col-md-8">{{ $order->additional_details }} </div>
                </div>
            </li>
        </ul>
        <div class="card-footer">
            <form method="POST" action="/orders/{{ $order->id }}">
                @method('DELETE')
                <input type="hidden" value="{{csrf_token()}}" name="_token">
                <div class="btn-group" role="group">
                    <a class="btn btn-info" role="button" href="/orders/{{ $order->id }}/edit">Modify</a>
                    <button type="submit" class="btn btn-danger">Delete</button>
                </div>
            </form>
        </div>
    </div>

@endsection