@extends('layouts.main')

@section('title', 'Orders\' List')

@section('content')

    <div class="card m-2">
        <div class="card-header font-weight-bold">
            Orders' list
        </div>
        <div class="card-body">
            <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col" class="d-none d-md-table-cell">Order ID</th>
                    <th scope="col">Name</th>
                    <th scope="col" class="d-none d-md-table-cell">E-mail</th>
                    <th scope="col">Actions</th>
                </tr>
                </thead>
                <tbody>

                @foreach ($orders as $order)
                    <tr>
                        <td scope="row" class="d-none d-md-table-cell">{{ $order->id }}</td>
                        <td>{{ $order->name }}</td>
                        <td class="d-none d-md-table-cell">{{ $order->email }}</td>
                        <td>
                            <form method="POST" action="/orders/{{ $order->id }}">
                                @method('DELETE')
                                <input type="hidden" value="{{csrf_token()}}" name="_token">
                                <div class="btn-group" role="group">
                                    <a class="btn btn-primary" role="button" href="/orders/{{ $order->id }}">View</a>
                                    <a class="btn btn-info" role="button" href="/orders/{{ $order->id }}/edit">Modify</a>
                                    <button type="submit" class="btn btn-danger">Delete</button>
                                </div>
                            </form>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
            </div>
        </div>
    </div>
@endsection