<?php

namespace Orders\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * OrderModel class
 *
 * Defines the model for the table Orders
 */
class OrderModel extends Model {

    /**
     * Table referred by the model
     *
     * @var string
     */
    protected $table = 'orders';

    protected $fillable = [ 'name', 'email', 'gender', 'address', 'telephone', 'additional_details' ];

}