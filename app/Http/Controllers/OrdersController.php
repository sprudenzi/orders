<?php

namespace Orders\Http\Controllers;

use Orders\Models\OrderModel;
use Illuminate\Http\Request;

class OrdersController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('orders/list', [ 'orders' => OrderModel::all() ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view( 'orders/create' );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'name'      => 'required|max:100',
            'email'     => 'required|max:100',
            'address'   => 'max:100',
            'telephone' => 'max:30',
        ]);

        $order = new OrderModel();
        $order->name                = $request->input('name');
        $order->email               = $request->input('email');
        $order->address             = $request->input('address');
        $order->telephone           = $request->input('telephone');
        $order->gender              = $request->input('gender');
        $order->additional_details  = $request->input('additional_details');
        $order->save();

        $request->session()->flash('message', 'Order successfully created');

        return redirect('/orders');
    }

    /**
     * Display the specified resource.
     *
     * @param  OrderModel  $order
     * @return \Illuminate\Http\Response
     */
    public function show(OrderModel $order)
    {
        return view( 'orders/show', [ 'order' => $order ] );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  OrderModel  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(OrderModel $order)
    {
        return view( 'orders/modify', $order );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  OrderModel  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OrderModel $order)
    {
        $request->validate([
            'name'      => 'required|max:100',
            'email'     => 'required|max:100',
            'address'   => 'max:100',
            'telephone' => 'max:30',
        ]);

        $order->name                = $request->input('name');
        $order->email               = $request->input('email');
        $order->address             = $request->input('address');
        $order->telephone           = $request->input('telephone');
        $order->gender              = $request->input('gender');
        $order->additional_details  = $request->input('additional_details');
        $order->save();

        $request->session()->flash('message', 'Order successfully updated');

        return redirect('/orders/' . $order->id );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  OrderModel  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, OrderModel $order)
    {
        try {
            $order->delete();
            $request->session()->flash('message', 'Order successfully deleted');
        } catch ( \Exception $e ) {
            $request->session()->flash('message', 'Order couldn\'t be deleted');
        }

        return redirect('/orders');
    }
}
