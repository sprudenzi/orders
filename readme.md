# Simple order project

To run you need to change the **.env-prod** file accordingly to your database settings and rename it as **.env**.

Then you should launch the commands:

```
    composer install
    php artisan migrate
    php artisan db:seed
    php artisan serve
```

Enjoy!

